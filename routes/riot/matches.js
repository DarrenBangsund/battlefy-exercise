const express = require("express");
const router = express.Router();
const http = require("../../util/http");

router.get("/getMatch/:matchId", function (req,res) {
    var url = "/lol/match/v3/matches/" + req.params.matchId;

    http.fetch(url)
    .then(http.getResponseWriter(res, 200))
    .catch(http.getResponseWriter(res, 400))
});

router.get("/getMatches/:summonerId", function (req,res) {
    var url = "/lol/match/v3/matchlists/by-account/" + req.params.summonerId + "/recent";

    http.fetch(url)
    .then(http.getResponseWriter(res, 200))
    .catch(http.getResponseWriter(res, 400))
});

module.exports = router;