const express = require("express");
const router = express.Router();

router.use("/summoner", require("./summoner.js"));
router.use("/match", require("./matches.js"));

module.exports = router;