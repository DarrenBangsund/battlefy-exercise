angular.module("app")
.directive("matchDirective", function() {
    return {
        "replace": true,
        "restrict": "E",
        "scope": {
            "summoner": "=",
            "match": "=",
            "items": "=",
            "runes": "=",
            "champions": "=",
        },
        "templateUrl": "public/views/match-directive.html",
        "controller": ['$scope', 'Match', function ($scope, Match) {
            if(!$scope.summoner) {
                console.error("Match directive is invalid. Summoner name required!");
            } else if(!$scope.match) {
                console.error("Match directive is invalid. Match name required!");
            }

            var match = new Match($scope.summoner, $scope.match);
            match.participant = getParticipant();
            
            function getParticipant() {
                var participant = {};

                for(var i =0; i < match.match.participantIdentities.length; i++) {
                    var id = match.match.participantIdentities[i];
                    if (id.player.accountId == $scope.summoner.accountId) {
                        participant.identity = id;
                        break;
                    }
                };

                for(var i = 0; i < match.match.participants.length; i++) {
                    var p = match.match.participants[i];
                    if (p.participantId == participant.identity.participantId) {
                        participant.p = p;
                        break;
                    }
                }

                var keys = Object.keys($scope.champions.data);
                for(var i = 0; i < keys.length; i++) {
                    var c = $scope.champions.data[keys[i]];
                    if (participant.p.championId == c.key) {
                        participant.champion = c;
                        break;
                    }
                }
                
                return participant;
            }

            function getKDR() {
                var kill = match.participant.p.stats.kills;
                var death = match.participant.p.stats.deaths;
                var max = Math.max(kill, death);

                var k = Math.round((kill/Math.max(kill, death)) * 100) / 100;
                var d = Math.round((death/Math.max(kill, death)) * 100) / 100;

                return k + ":" + d;
            }

            function convertTime() {
                var t = match.match.gameDuration;
                var mins = Math.floor(t/60);
                var sec = t - mins * 60;

                return mins + ":" + sec;
            }
            //assign scope
            $scope.match = match;
            $scope.matchOutcome = match.participant.p.stats.win ? "WIN" : "LOSS";
            $scope.kdr = getKDR();
            $scope.runningMins = convertTime();
            $scope.numCreeps = match.participant.p.stats.totalMinionsKilled + match.participant.p.stats.neutralMinionsKilled;
            $scope.creepsPerMin = Math.round(10*($scope.numCreeps/(match.match.gameDuration/60)))/10;
            // $scope.champion = 
        }]
    }
})