angular.module("app")
.service("Match", ["leagueService", function(leagueService) {
    
    m = function (summoner, match) {
        this.match = match;

    }

    return function (summonerName, match) {
        return new m(summonerName, match);
    }
}])