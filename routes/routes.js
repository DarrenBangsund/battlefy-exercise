const express = require("express");
const router = express.Router();

router.use("/riot", require("./riot/riot.js"));

module.exports = router;