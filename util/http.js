const rp = require("request-promise");
const config = require("../config.json");

module.exports = {
    fetch: function(url) {
        var url = "https://na1.api.riotgames.com" + url + "?api_key=" + config.apiKey
        return rp(url);
    },
    getResponseWriter: function(res) {
        return function (body) {
            res.status(body.statusCode || 200).send(body)
        };
    }
}