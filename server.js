const express = require("express");
const path = require("path");
const request = require("request");
const async = require("async");
const config = require("./config.json");
const apiKey = config.apiKey;

const app = express();

//staticly serve the public folder
app.use('/async', express.static(__dirname + '/node_modules/async'));
app.use('/public', express.static(__dirname + '/public'));

//configure rendering engine
app.set('view engine', 'ejs');
app.set("views", "./views");

app.use("/api", require("./routes/routes.js"));

app.get("*", (req, res) => {
    res.render('index', { title: 'Express' })
})

app.listen(3000, () => console.log("Listening on port 3000"))