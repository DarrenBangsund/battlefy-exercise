angular.module("app")
.service("doerService", ["$http", "$q", "API", function($http, $q, API) {
    fetch = function(verb, url) {
        return $http({
            method: verb,
            url: url
        })
    }

    var d = function(apiUrl, noAPI) {
        this.API = (noAPI ? "" : API) + apiUrl;
    }

    d.prototype.get = function(url) {
        return fetch("GET", this.API + url)
    }

    //these methods aren't really needed for this exercise
    //so I didn't spend too much time fleshing it out.
    // 
    // d.prototype.post = (url) => {
    //     return fetch("POST", this.API + url)
    // }
    // d.prototype.delete = (url) => {
    //     return fetch("DELETE", this.API + url)
    // }
    // d.prototype.put = (url) => {
    //     return fetch("PUT", this.API + url)
    // }

    return function(apiUrl, noAPI) {
        return new d(apiUrl, noAPI);
    }
}])