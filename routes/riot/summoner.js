const express = require("express");
const router = express.Router();
const http = require("../../util/http");
const config = require("../../config.json");

router.get("/getSummonerByName/:username", function (req,res) {
    var url = "/lol/summoner/v3/summoners/by-name/";

    http.fetch(url + req.params.username)
    .then(http.getResponseWriter(res))
    .catch(http.getResponseWriter(res))
});

module.exports = router;