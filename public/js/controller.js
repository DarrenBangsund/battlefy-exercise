angular.module("app")
  .controller("listCtrl", ["$timeout", "$mdToast", "leagueService", "doerService", function ($timeout, $mdToast, leagueService, doerService) {
    ctrl = this;
    var timeout = undefined;
    var l = new leagueService();
    var dd = new doerService("http://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US", true);

    //scope
    ctrl.isLoading = false;
    ctrl.user = undefined;
    ctrl.fetch = function () {
      if (ctrl.user == undefined) return;
      if (timeout != undefined) $timeout.cancel(timeout);

      timeout = $timeout(function () {
        if(ctrl.user == "") {
          ctrl.summoner = {};
          ctrl.matches = [];
          return;
        }

        ctrl.isLoading = true;
        async.waterfall([
          function (waterfallCb) {

            l.getSummonerByName(ctrl.user)
              .then(function (data) {
                ctrl.summoner = data.data;
                return waterfallCb(undefined, ctrl.summoner);
              })
              .catch(waterfallCb)

          }, function (summoner, waterfallCb) {

            l.getMatches(summoner)
              .then(function (matches) {
                return waterfallCb(undefined, matches);
              })
              .catch(waterfallCb)

          }, function (matches, waterfallCb) {
            var out = [];

            async.forEachSeries(matches.data.matches, function (match, feCb) {
              l.getMatch(match.gameId)
                .then(function (m) {
                  out.push(m.data);
                  return feCb();
                })
                .catch(feCb)
            }, function (err) {
              if (err) return waterfallCb(err);

              ctrl.matches = out;
              return waterfallCb();
            });
          }, function (waterfallCb) {
            var static = {
              "items": "/item.json",
              "runes": "/rune.json",
              "champions": "/champion.json",
            }

            //gather static lists
            async.forEachOfSeries(static, function(val, key, cb) {
              dd.get(val)
              .then(function (res) {
                ctrl[key] = res.data;
              })
              .then(cb)
            }, waterfallCb) 
          }
        ], function (err) {
          if(err) {
            console.error(err);
            ctrl.summoner = {};
            ctrl.matches = [];
            $mdToast.show($mdToast.simple().textContent(err.status + ": " + err.statusText));
          }
          ctrl.isLoading = false;
        })
      }, 1500)
    }
  }])