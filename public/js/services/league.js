angular.module("app")
.service("leagueService", ["doerService", function(doerService) {
    
    l = function (summonerName) {
        this.doer = new doerService("/riot");
        this.summonerName = summonerName;
    }
    
    l.prototype.getSummonerByName = function (summonerName) {
        var summonerName = (!summonerName) ? this.summonerName : summonerName;
        if (!summonerName) return;

        return this.doer.get("/summoner/getSummonerByName/" + summonerName);
    }

    l.prototype.getMatches = function (summonerData) {
        if(!summonerData) return;

        return this.doer.get("/match/getMatches/" + summonerData.accountId);
    }

    l.prototype.getMatch = function (matchID) {
        if(!matchID) return;

        return this.doer.get("/match/getMatch/" + matchID);
    }

    return function (summonerName) {
        return new l(summonerName);
    }
}])